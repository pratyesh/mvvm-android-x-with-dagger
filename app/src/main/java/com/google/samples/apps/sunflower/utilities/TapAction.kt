/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.utilities

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

class TapAction<TSender> internal constructor(
    private val getSender: () -> TSender
) {
  companion object {
    fun <TSender> create(getSender: () -> TSender): TapAction<TSender>
        = TapAction(getSender)
  }

  private val onTap = PublishRelay.create<TSender>()
  val observe: Observable<TSender> get() = onTap

  fun perform() = onTap.accept(getSender())
}
