/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.samples.apps.sunflower.R
import com.google.samples.apps.sunflower.adapters.RecentPhotosAdapter
import com.google.samples.apps.sunflower.databinding.RecentPhotosBinding
import com.google.samples.apps.sunflower.details.PhotoDetailsActivity
import com.google.samples.apps.sunflower.utilities.DaggerFragment
import com.miguelcatalan.materialsearchview.MaterialSearchView
import javax.inject.Inject

class RecentPhotosFragment : DaggerFragment() {
    @Inject
    lateinit var viewModel: RecentPhotosViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

        viewModel.loadPhotos(savedInstanceState)
        viewModel.onErrorLoadingPhotos.subscribe {
            view?.let {
                Snackbar
                        .make(it, R.string.cannot_load_photos, Snackbar.LENGTH_LONG)
                        .setAction(R.string.retry) { viewModel.retry() }
                        .show()
            }
        }
        viewModel.onPhotoTapped.subscribe {
            startActivity(PhotoDetailsActivity.newIntent(requireActivity(), it))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.onSaveInstanceState(outState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = RecentPhotosBinding.inflate(inflater, container, false)

        binding.swipeRefreshLayout.isEnabled = false
        binding.toolbar.inflateMenu(R.menu.recent_photos)
        binding.searchView.setMenuItem(binding.toolbar.menu.findItem(R.id.search))
        binding.searchView.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = viewModel.onQueryTextSubmit(query)

            override fun onQueryTextChange(newText: String?): Boolean = false
        })

        // To configure columns for different orientations.
        // e.g. for portrait, column count is 2 while for landscape, the count is 4.
//        binding.photosView.layoutManager = LayoutManagers.grid(resources.getInteger(R.integer.galleryColumns)).create(binding.photosView)
        addDividers(binding.photosView)

//        binding.photoItemBinding = ItemBinding.of<PhotoViewModel>(BR.viewModel, R.layout.photo)

        binding.viewModel = viewModel

        val adapter = RecentPhotosAdapter()
        binding.photosView.adapter = adapter

        return binding.root
    }

    private fun addDividers(photosView: RecyclerView) {
        val verticalDividerItemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL).apply {
            setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.vertical_divider)!!)
        }
        photosView.addItemDecoration(verticalDividerItemDecoration)

        val horizontalDividerItemDecoration = DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL).apply {
            setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.horizontal_divider)!!)
        }
        photosView.addItemDecoration(horizontalDividerItemDecoration)
    }

    override fun onDestroy() {
        viewModel.onCleared()
        super.onDestroy()
    }
}
