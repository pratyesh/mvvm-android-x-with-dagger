/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.network;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import org.immutables.gson.Gson;
import org.immutables.value.Value;
import org.jetbrains.annotations.Nullable;

@Value.Immutable
@Gson.TypeAdapters
@JsonAdapter(GsonAdaptersPhotoEntity.class)
interface PhotoEntity {
  String id();
  String secret();
  String server();
  int farm();
  @Nullable String title();
  @SerializedName("url_l") @Nullable String url_l();
  @SerializedName("url_o") @Nullable String url_o();
  @SerializedName("url_c") @Nullable String url_c();
  @SerializedName("url_z") @Nullable String url_z();
  @SerializedName("url_n") @Nullable String url_n();
  @SerializedName("url_m") @Nullable String url_m();
  @SerializedName("url_q") @Nullable String url_q();
  @SerializedName("url_s") @Nullable String url_s();
  @SerializedName("url_t") @Nullable String url_t();
  @SerializedName("url_sq") @Nullable String url_sq();
}
