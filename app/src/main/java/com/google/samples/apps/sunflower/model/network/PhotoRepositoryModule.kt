/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.network

import android.content.Context
import com.google.samples.apps.sunflower.data.AppDatabase
import com.google.samples.apps.sunflower.model.local.PhotoRepository
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class PhotoRepositoryModule {
  @Provides
  @Singleton
  fun getPhotoRepository(context: Context): PhotoRepository {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY

    val httpClient = OkHttpClient.Builder()
        .addInterceptor(logging)
        .build()

    val api = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://api.flickr.com/")
        .client(httpClient)
        .build()
        .create(FlickrApi::class.java)

//    val appDatabase = createAppDatabase(context)
    val appDatabase = AppDatabase.getInstance(context)
    return PhotoRepositoryImpl(
        api,
        PhotoMapper(),
        PhotoEntityMapper(),
        appDatabase.photoDao()
    )
  }

//  private fun createAppDatabase(context: Context): AppDatabase =
//      Room.databaseBuilder<AppDatabase>(
//          context,
//          AppDatabase::class.java,
//          "flickr.sqlite"
//      ).build()
}
