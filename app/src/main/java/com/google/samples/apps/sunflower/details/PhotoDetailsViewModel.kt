/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.details

import android.net.Uri
import android.os.Bundle
import androidx.databinding.ObservableField
import com.google.samples.apps.sunflower.model.local.GetOriginalPhoto
import com.google.samples.apps.sunflower.utilities.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

const val KEY_PHOTO_ID = "photoId"

class PhotoDetailsViewModel @Inject internal constructor(
    private val getOriginalPhoto: GetOriginalPhoto
) : BaseViewModel() {
  val title = ObservableField<String>()
  val link = ObservableField<Uri>()

  fun extractPhotoId(arguments: Bundle?) {
    val photoId = arguments?.getString(KEY_PHOTO_ID)
    checkNotNull(photoId) {
      "Must specify photo id"
    }

    photoId.let {
      getOriginalPhoto(it)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe { (photo, size) ->
            title.set(photo.title)
            link.set(Uri.parse(size.link))
          }
          .autoDispose()
    }
  }
}
