/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.sunflower.list.RecentPhotosFragment
import com.google.samples.apps.sunflower.PlantListFragmentDirections
import com.google.samples.apps.sunflower.databinding.PhotoBinding
import com.google.samples.apps.sunflower.list.PhotoViewModel

/**
 * Adapter for the [RecyclerView] in [RecentPhotosFragment].
 */
class RecentPhotosAdapter : ListAdapter<PhotoViewModel, RecentPhotosAdapter.ViewHolder>(PhotosDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = getItem(position)
        holder.apply {
//            bind(createOnClickListener(photo.id), photo)
            itemView.tag = photo
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(PhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    private fun createOnClickListener(plantId: String): View.OnClickListener {
        return View.OnClickListener {
            val direction = PlantListFragmentDirections.actionPlantListFragmentToPlantDetailFragment(plantId)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(private val binding: PhotoBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, item: PhotoViewModel) {
            binding.apply {
//                clickListener = listener
                viewModel = item
                executePendingBindings()
            }
        }
    }
}

private class PhotosDiffCallback : DiffUtil.ItemCallback<PhotoViewModel>() {

    override fun areItemsTheSame(oldItem: PhotoViewModel, newItem: PhotoViewModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PhotoViewModel, newItem: PhotoViewModel): Boolean {
        return oldItem == newItem
    }
}