/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.local

import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

open class GetOriginalPhoto @Inject internal constructor(
    private val photoRepository: PhotoRepository
) {
  operator fun invoke(photoId: String): Single<Pair<Photo, PhotoSize>> =
      photoRepository.getPhotoById(photoId)
          .zipWith(
              photoRepository.getOriginalPhotoSize(photoId),
              BiFunction { photo, size -> Pair(photo, size) }
          )
}
