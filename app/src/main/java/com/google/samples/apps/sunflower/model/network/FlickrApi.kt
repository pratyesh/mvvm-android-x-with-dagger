/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

internal const val URLS = "url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o"

internal interface FlickrApi {
  @GET("services/rest/?method=flickr.photos.getRecent&nojsoncallback=1&format=json")
  fun getRecent(
          @Query("api_key") apiKey: String,
          @Query("extras") extras: String = URLS
  ): Single<PhotosResponseEntity>

  @GET("services/rest/?method=flickr.photos.search&nojsoncallback=1&format=json")
  fun search(
      @Query("api_key") apiKey: String,
      @Query("text") text: String? = null,
      @Query("extras") extras: String = URLS
  ): Single<PhotosResponseEntity>
}
