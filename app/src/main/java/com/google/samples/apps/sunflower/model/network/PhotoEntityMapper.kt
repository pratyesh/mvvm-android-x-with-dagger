/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.network

open internal class PhotoEntityMapper {
    open operator fun invoke(entity: PhotoDbEntity?): PhotoEntity? = entity?.let {
        ImmutablePhotoEntity.builder().apply {
            id(it.id)
            secret(it.secret)
            server(it.server)
            farm(it.farm)
            title(it.title)
            url_l(it.url_l)
            url_o(it.url_o)
            url_c(it.url_c)
            url_z(it.url_z)
            url_n(it.url_n)
            url_m(it.url_m)
            url_q(it.url_q)
            url_s(it.url_s)
            url_t(it.url_t)
            url_sq(it.url_sq)
        }.build()
    }

    open fun toDbEntities(entities: List<PhotoEntity>): List<PhotoDbEntity> =
            entities.map {
                PhotoDbEntity(
                        id = it.id(),
                        secret = it.secret(),
                        server = it.server(),
                        farm = it.farm(),
                        title = it.title(),
                        url_l = it.url_l(),
                        url_o = it.url_o(),
                        url_c = it.url_c(),
                        url_z = it.url_z(),
                        url_n = it.url_n(),
                        url_m = it.url_m(),
                        url_q = it.url_q(),
                        url_s = it.url_s(),
                        url_t = it.url_t(),
                        url_sq = it.url_sq()
                )
            }
}
