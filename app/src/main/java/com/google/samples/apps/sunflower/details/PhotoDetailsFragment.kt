/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.drawable.ProgressBarDrawable
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.google.samples.apps.sunflower.utilities.DaggerFragment
import javax.inject.Inject
import com.google.samples.apps.sunflower.databinding.PhotoDetailsBinding

class PhotoDetailsFragment : DaggerFragment() {
  @Inject lateinit var viewModel: PhotoDetailsViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    retainInstance = true

    viewModel.extractPhotoId(arguments)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val binding = PhotoDetailsBinding.inflate(inflater, container, false)
    binding.toolbar.setNavigationOnClickListener {
      requireActivity().finish()
    }

    // See http://frescolib.org/docs/using-simpledraweeview.html.
    val hierarchy = GenericDraweeHierarchyBuilder.newInstance(resources)
        .setProgressBarImage(ProgressBarDrawable())
        .build()
    binding.photoView.hierarchy = hierarchy

    binding.viewModel = viewModel
    return binding.root
  }

  override fun onDestroy() {
    viewModel.onCleared()
    super.onDestroy()
  }
}
