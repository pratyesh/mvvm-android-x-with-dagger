/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.network

import com.google.samples.apps.sunflower.model.local.Photo
import com.google.samples.apps.sunflower.model.local.PhotoSize

internal open class PhotoMapper {
    open operator fun invoke(entities: List<PhotoEntity>): List<Photo> =
            entities
                    .filter { it.thumbnailUrl() != null }
                    .map { toPhoto(it) }

    open fun toPhoto(entity: PhotoEntity): Photo = Photo(
            id = entity.id(),
            link = checkNotNull(entity.thumbnailUrl()) {
                "Thumbnail url doesn't exist"
            },
            title = entity.title()
    )

    open fun toPhotoSize(entity: PhotoEntity): PhotoSize = PhotoSize(
            width = 0,
            height = 0,
            link = checkNotNull(entity.originalUrl()) {
                "Original url doesn't exist"
            }
    )

    private fun PhotoEntity.thumbnailUrl() = with(this) {
        url_l() ?: url_o() ?: url_c() ?: url_z() ?: url_n() ?: url_m()
        ?: url_q() ?: url_s() ?: url_t() ?: url_sq()
    }

    private fun PhotoEntity.originalUrl() = with(this) {
        url_o() ?: url_l() ?: url_c() ?: url_z() ?: url_n() ?: url_m()
        ?: url_q() ?: url_s() ?: url_t() ?: url_sq()
    }
}
