/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.model.network

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photos")
class PhotoDbEntity(
        @PrimaryKey var id: String = "",
        @ColumnInfo(name = "secret") var secret: String = "",
        @ColumnInfo(name = "server") var server: String = "",
        @ColumnInfo(name = "farm") var farm: Int = 0,
        @ColumnInfo(name = "title") var title: String? = null,
        @ColumnInfo(name = "url_l") var url_l: String? = null,
        @ColumnInfo(name = "url_o") var url_o: String? = null,
        @ColumnInfo(name = "url_c") var url_c: String? = null,
        @ColumnInfo(name = "url_z") var url_z: String? = null,
        @ColumnInfo(name = "url_n") var url_n: String? = null,
        @ColumnInfo(name = "url_m") var url_m: String? = null,
        @ColumnInfo(name = "url_q") var url_q: String? = null,
        @ColumnInfo(name = "url_s") var url_s: String? = null,
        @ColumnInfo(name = "url_t") var url_t: String? = null,
        @ColumnInfo(name = "url_sq") var url_sq: String? = null
)
