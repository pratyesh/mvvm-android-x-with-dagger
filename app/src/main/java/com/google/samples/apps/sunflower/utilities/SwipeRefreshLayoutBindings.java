/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.utilities;


import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

@BindingMethods({
        @BindingMethod(
                type = SwipeRefreshLayout.class,
                attribute = "android:onRefresh",
                method = "setOnRefreshListener"
        )
})
public final class SwipeRefreshLayoutBindings {
    private SwipeRefreshLayoutBindings() {
    }

    @BindingAdapter("refreshing")
    public static void setRefreshing(final SwipeRefreshLayout v, final boolean refreshing) {
        // If we invoke setRefreshing(true) while the SwipeRefreshLayout isn't added to the root view,
        // its progress indicator won't be shown up. In order to deal with that, we schedule the first
        // call of setRefreshing() until it's added to the root view via this post().
        v.post(() -> v.setRefreshing(refreshing));
    }
}
